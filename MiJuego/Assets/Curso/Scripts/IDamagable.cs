using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CursoIqUcacue {

    public interface IDamagable {
        
        int PuntosVida { get; set; }

        void AplicarDanio(int cantidad);
    }
}