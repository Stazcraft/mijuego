using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CursoIqUcacue {

    // Lista enumerada con los tipos de movimiento que puede realizar el objetivo
    public enum Movimiento{
        ArribaAbajo,        // Movimiento de arriba a abajo
        IzquierdaDerecha    // Movimiento de izquierda a derecha
    }

    public class ObjetivoMovil : MonoBehaviour {
        
        [Header("Configuración objetivo móvil")]
        // Tipo de movimiento que realiza el objetivo
        [SerializeField] private Movimiento tipoMovimiento = Movimiento.ArribaAbajo;

        // Valor de alcance a la que se desplaza el objetivo
        [SerializeField] private float rango = 1;

        // Tiempo que debe transcurrir para que el objetivo se mueva de un punto hacia otro
        [SerializeField] [Range(1, 10)] private float tiempo = 3;


        // Tiempo actual transucrrido durante el movimiento
        private float tiempoTrasncurrido;

        // Punto inicial de referencia
        private Vector2 puntoPartida;

        // Punto destino de referencia
        private Vector2 puntoDestino;

        // Punto de control o guardado
        private Vector2 puntoGuardado;



        // Método de llamada de Unity, se llama un única vez al iniciar el aplicativo después de Awake
        // Permite configurar el movimiento del objetivo
        private void Start(){
            AsignarPuntosMovimiento();
        }


        // Método de llamada de Unity, se llamada en cada frame del computador
        // Se realiza la lógica de movimiento del objetivo
        private void Update(){
            tiempoTrasncurrido += Time.deltaTime;
            float porcentaje = tiempoTrasncurrido / tiempo;
            transform.position = Vector3.Lerp(puntoPartida, puntoDestino, porcentaje);
            float distance = Vector3.Distance(transform.position, puntoDestino);
            if(distance <= 0.01f){
                tiempoTrasncurrido = 0;
                puntoPartida = puntoDestino;
                puntoDestino = puntoGuardado;
                puntoGuardado = puntoPartida;
            }
        }


        // Método privado que permite asignar el movimiento al objetivo y configurarlo
        private void AsignarPuntosMovimiento(){
            if(tipoMovimiento == Movimiento.ArribaAbajo){
                puntoPartida = (Vector2)transform.position + Vector2.up * rango;
                puntoDestino = (Vector2)transform.position + Vector2.down * rango;
            }else{
                puntoPartida = (Vector2)transform.position + Vector2.left * rango;
                puntoDestino = (Vector2)transform.position + Vector2.right * rango;
            }
            puntoGuardado = puntoPartida;
        }
    }
}