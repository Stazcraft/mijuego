using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CursoIqUcacue {

    public class Objetivo : MonoBehaviour, IDamagable {
        
        [Header("Configuración del objetivo")]
        // Puntos de vida del objetivo
        [SerializeField] private int puntosVida = 10;

        // Si es verdadero el objeto ha sido destruido, caso contrario esta con vida
        private bool objetoDestruido = false;

        // Referencia al script 'ControladorJuego'
        private ControladorJuego controladorJuego;


        // Referencia heredada de la interfaz para los puntos de vida
        public int PuntosVida {
            get => puntosVida;
            set {
                puntosVida = value;
                if(puntosVida <= 0){
                    puntosVida = 0;
                    objetoDestruido = true;
                }
            }
        }



        // Método público que permite al objetivo recibir daño
        public void AplicarDanio(int cantidad) {
            PuntosVida -= cantidad;
            if(objetoDestruido) {
                if(controladorJuego != null) controladorJuego.ContarObjetivosDestruidos();
                Destroy(gameObject);
            }
        }


        public ControladorJuego AgregarControlador {
            set => controladorJuego = value;
        }
    }
}