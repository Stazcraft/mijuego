using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


namespace CursoIqUcacue {

    public class ControladorJuego : MonoBehaviour {

        [Header("Configuración del juego")]
        // Tiempo que dura el juego antes de perder
        [SerializeField] private float tiempoJuego = 60;

        // Mensaje del juego cuando este termine con exito
        [SerializeField] private string mensajeFinJuegoExito = "Congratulation";

        // Mensaje del juego cuando este termine con derrota
        [SerializeField] private string mensajeFinJuegoDerrota = "Game Over";
        

        [Header("Paneles de UI")]
        // Referencia del gameobject donde se encuentra los objetivos
        [SerializeField] private GameObject panelFinJuego;

        // Referencia del gameobject donde se encuentra los objetivos
        [SerializeField] private GameObject objetivos;

        // Referencia del boton volver a jugar
        [SerializeField] private Button botonVolverJugar;

        // Referencia del boton salir
        [SerializeField] private Button botonSalir;

        // Referencia del texto que permite mostrar el fin del juego
        [SerializeField] private TMP_Text textoFinJuego;

        // Texto que forma parte de la UI - se utiliza para mostrar el tiempo transcurrido
        [SerializeField] private TMP_Text textoTemporizador;

        // Referencia del script del arma
        [SerializeField] private Arma arma;



        // Se utiliza para saber la cantidad de objetivos en la escena
        private int contador = 0;

        // Referencia del indice de la escena
        private int indiceEscenario;

        // Referencia del temporizador
        private float tiempoPorFrame = 0f;
        private float tiempoPorSegundo = 0f;
        private readonly float lineaDeTiempo = 1;
        private bool iniciar = true;



        // Método de llamada de Unity, se llama un única vez al iniciar el aplicativo después de Awake
        // Se procede a configurar el administrador del juego
        private void Start(){
            foreach(Transform actual in objetivos.transform){
                Objetivo objetivo = actual.GetComponent<Objetivo>();
                if(objetivo != null){
                    objetivo.AgregarControlador = this;
                    contador++;
                }
            }
            panelFinJuego.SetActive(false);

            // Obtén el índice de la escena actual
            indiceEscenario = SceneManager.GetActiveScene().buildIndex;

            // Carga la escena actual de nuevo para reiniciar el nivel
            botonVolverJugar.onClick.AddListener( () => {
                Time.timeScale = 1;
                SceneManager.LoadScene(indiceEscenario);
            } );
            botonSalir.onClick.AddListener( () => Application.Quit() );

            if(!textoTemporizador) textoTemporizador.text = "00:00:00 seg.";
            arma.enabled = true;
        }


        // Método de llamada de Unity, se llamada en cada frame del computador
        // Se realiza la lógica de actualización del aplicativo
        private void Update(){
            if(iniciar){
                tiempoPorFrame = Time.deltaTime * lineaDeTiempo;
                tiempoPorSegundo += tiempoPorFrame;
                ActualizarReloj(tiempoPorSegundo);
                if(tiempoPorSegundo >= tiempoJuego){
                    ValidarJuego(mensajeFinJuegoDerrota);
                }
            }
        }


        // Método público que permite contar objetivos destruidos
        public void ContarObjetivosDestruidos(){
            contador--;
            if(contador == 0) ValidarJuego(mensajeFinJuegoExito);
            
        }


        // Método privado que permite validar el estado del juego
        private void ValidarJuego(string mensaje){
            textoFinJuego.text = mensaje;
            panelFinJuego.SetActive(true);
            iniciar = false;
            arma.enabled = false;
            Time.timeScale = 0;
        }


        // Método empleado para actualizar el tiempo en horas, minutos y segundos
        private void ActualizarReloj(float tiemInSeconds){
            int hours;
            int minutes;
            int seconds;
            if(tiemInSeconds < 0){
                tiemInSeconds = 0;
            }
            hours = (int) tiemInSeconds / 3600;
            minutes = (int) (tiemInSeconds - (hours * 3600)) / 60;
            // minutes = (int) tiemInSeconds/ 60;
            seconds = (int) tiemInSeconds % 60;
            textoTemporizador.text = string.Format("{0:00}:{1:00}:{2:00} seg.", hours, minutes, seconds);
        }
    }
}