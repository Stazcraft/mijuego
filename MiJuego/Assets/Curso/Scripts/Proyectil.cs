using UnityEngine;

namespace CursoIqUcacue {

    // Componente requerido para que el script funcione
    [RequireComponent(typeof(Rigidbody2D))]

    public class Proyectil : MonoBehaviour {

        [Header("Configuración del proyectil")]
        // Daño del arma, a mayor cantidad mayor el daño que inflige
        [SerializeField] private int danio = 1;


        [Header("Ajustes de tiempo del proyectil")]
        // Tiempo de vida del proyectil cuando es disparado
        [SerializeField] private float tiempoDuracion = 7.0f;

        // Tiempo de duración del proyectil al momento de que golpea con un objetivo
        [SerializeField] private float tiempoDuracionGolpearObjetivo = 0.1f;

        // Tiempo de duración del proyectil al momento de que golpea con el terreno
        [SerializeField] private float tiempoDuracionGolpearTerreno = 2f;


        [Header("Condiciones del proyectil")]
        // Si es verdadero, el proyectil se queda anclado en el lugar donde golpea
        [SerializeField] private bool anclarDondeGolpea = false;

        // Si es verdadero el proyectil puede realizar un efecto de explosión
        [SerializeField] private bool usarExplosionEnProyectil = false;

        // Referencia del prefab que permite al proyectil explotar
        [SerializeField] private GameObject prefabExplosion;


        // Si es verdadero el proyectil ha golpeado con una colisión, caso contrario n
        private bool haGolpeado = false;
        
        // Referencia al componente de fisicas del objeto
        private Rigidbody2D fisicas2D;


        // Método de llamada de Unity, se llama un única vez al iniciar el aplicativo
        // Se referencia el componente de físicas
        private void Awake(){
            fisicas2D = GetComponent<Rigidbody2D>();
        }


        // Método de llamada de Unity, se llama al habilitarse el script, después de Awake
        // Sirve para destruir el proyectil despues de un tiempo apenas es instanciado
        private void OnEnable() {
            fisicas2D.constraints = RigidbodyConstraints2D.FreezeRotation;
            Destroy(gameObject, tiempoDuracion);
        }


        // Si se deshabilita el script, se destruye el proyectil
        private void OnDisable() {
            Destroy(gameObject);
            if(haGolpeado && usarExplosionEnProyectil) InstanciarExplosion();

        }


        // Método de llamada de Unity, se llamada cada cierto intervalo de tiempo
        // Se realiza la lógica de actualización del proeyctil
        private void FixedUpdate() {
            if(!haGolpeado) ActualizarPosicionCaida();
        }


        // Método privado que permite calcular la dirección del proyectil
        private void ActualizarPosicionCaida(){
            Vector2 direccion = fisicas2D.velocity;
            float anguloCaida = Mathf.Atan2(direccion.y, direccion.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(anguloCaida, Vector3.forward);
        }


        // método de llamada de unity, se activa en el momento que el objeto colisiona con otro
        private void OnCollisionEnter2D(Collision2D other) {
            if(!other.gameObject.CompareTag("Untagged")){
                haGolpeado = true;
                fisicas2D.isKinematic = anclarDondeGolpea;
                if(anclarDondeGolpea){
                    fisicas2D.velocity = Vector2.zero;
                }
                bool golpeoObjetivo = false;
                if(other.gameObject.TryGetComponent(out IDamagable damagable)){
                    damagable.AplicarDanio(danio);
                    golpeoObjetivo = true;
                }
                Destroy(gameObject, golpeoObjetivo ? tiempoDuracionGolpearObjetivo: tiempoDuracionGolpearTerreno);
            }
        }


        // Método privado que permite instanciar la explosión del proeyctil
        private void InstanciarExplosion(){
            Instantiate(prefabExplosion, transform.position, Quaternion.identity);
        }
    }
}