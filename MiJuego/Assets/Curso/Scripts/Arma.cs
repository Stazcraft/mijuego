using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace CursoIqUcacue {

    public class Arma : MonoBehaviour {
        
        [Header("Configuración del arma")]
        // Alcance del arma, a mayor rango, más lejos pueden llegar los proyectiles
        [SerializeField] private float rangoDisparo = 10;

        // Potencia de disparo del arma, a mayor rango más lejos viaja el proyectil
        [SerializeField] private float potenciaDisparo = 10;


        [Header("Referencia del Proyectil")]
        // Referencia del objeto prefabricado del proyectil
        [SerializeField] private GameObject prefabProyectil;


        [Header("Configuración de alcance del arma")]
        // Referencia del objeto prefabricado de los puntos por donde pasa el proyectil
        [SerializeField] private GameObject prefabPunto;

        // Cantidad de puntos de control a mostrar en el arma, a mayor cantidad más preciso
        [SerializeField] [Range(1, 1100)] private int numeroPuntosMostrar = 15;

        // Espacios entre un punto a otro
        [SerializeField] [Range(0.01f, 1.0f)] private float espacioEntrePunto = 0.1f;


        // [Header("Configuración de tecla de disparo")]
        // Referencia del acceso a la tecla de disparo
        // [SerializeField] private KeyCode botonDisparo = KeyCode.Space;

                
        // Direción hacia donde debe apuntar el arma
        private Vector2 direccion;
        
        // Arreglo de los puntos de control que se crean para determinar el rango de disparo
        private GameObject[] puntos;

        // Referencia a la cámara principal de la escena
        private Camera mainCamera;

        // Referencia al componente de animacion
        private Animator controladorAnimacion;



        // Método de llamada de Unity, se llama un única vez al iniciar el aplicativo
        // Se instancias las variables necesarias para el funcionamiento del script
        private void Awake(){
            mainCamera = Camera.main;
            controladorAnimacion = GetComponent<Animator>();
        }


        // Método de llamada de Unity, se llama un única vez al iniciar el aplicativo después de Awake
        // Se configuran y crean los puntos de referencia del alcance del arma
        private void Start(){
            puntos = new GameObject[numeroPuntosMostrar];
            for(int i = 0; i < numeroPuntosMostrar; i++){
                puntos[i] = Instantiate(prefabPunto, transform.position, quaternion.identity);
                puntos[i].transform.parent = transform;
            }
        }


        // Si se deshabilita el script, se destruye los puntos de referencia del alcance del arma
        private void OnDisable() {
            for(int i = 0; i < numeroPuntosMostrar; i++){
                Destroy(puntos[i]);
            }
            puntos = null;
        }


        // Método de llamada de Unity, se llamada en cada frame del computador
        // Se realiza la lógica de actualización del aplicativo
        private void Update(){
            // Se verifica que la tecla de acceso se ha precionado para poder disparar
            if(Input.GetMouseButtonDown(0)){
                Disparar();
            }

            // Se actualiza la rotación del arma en base a la posición del ratón
            ActualizarRotacionArma();
        }


        // Método privado que permite actualizar la rotación del arma en función al ratón
        private void ActualizarRotacionArma(){
            Vector2 posicionRaton = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            direccion = posicionRaton - (Vector2)transform.position;
            transform.right = direccion;
            for(int i = 0; i < puntos.Length; i++){
                puntos[i].transform.position = PosicionPuntosDisparo(i * espacioEntrePunto);
            }
        }


        // Método privado que permite ajustar los puntos de referencia en base al alcace del arma
        private Vector2 PosicionPuntosDisparo(float t){
            return (Vector2)transform.position + (rangoDisparo * t * direccion.normalized) + (0.5f * Physics2D.gravity * (t*t));
        }


        // Método privado que permite instanciar y disparar el proyectil del arma
        private void Disparar(){
            if(controladorAnimacion != null) controladorAnimacion.SetTrigger("Disparar");
            GameObject nuevoProyectil = Instantiate(prefabProyectil, transform.position, transform.rotation);
            nuevoProyectil.GetComponent<Rigidbody2D>().velocity = transform.right * potenciaDisparo;
        }
    }
}