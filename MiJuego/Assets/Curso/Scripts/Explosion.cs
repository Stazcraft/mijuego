using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CursoIqUcacue {

    public class Explosion : MonoBehaviour {
        

        [Header("Configuración de la explosión")]
        // Radio de explosión
        [SerializeField] private float radioDeExplosion = 5.0f;

        // Fuerza de explosión
        [SerializeField] private float fuerzaDeExplosion = 1000.0f;

        // Daño que realiza la explosión
        [SerializeField] private int danio = 50;
        
        // Tiempo que dura la explosión
        private readonly float tiempo = 1.0f;

        private void OnEnable(){
            // Destruye la bomba después de la explosión
            Destroy(gameObject, tiempo);

            Debug.Log("HEJEJEJ");
        }


        // Método de llamada de Unity, se llama un única vez al iniciar el aplicativo después de Awake
        // Se realiza la lógica de detección de de objetos a los cuales realizar daño
        private void Start() {
            // Detecta colisiones cuando la bomba entra en contacto con algo
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radioDeExplosion);

            foreach (Collider2D collider in colliders) {
                // Calcula la dirección desde el centro de la explosión al objeto
                Vector2 direccion = collider.transform.position - transform.position;

                // Calcula la distancia desde el objeto al centro de la explosión
                float distancia = direccion.magnitude;

                // Aplica una fuerza a los objetos en el radio de explosión
                if (collider.TryGetComponent(out Rigidbody2D rb)) {
                    float fuerza = 1 - (distancia / radioDeExplosion);
                    rb.AddForce(fuerzaDeExplosion * fuerza * direccion.normalized);
                }

                // Aplica daño a los objetos que tengan un componente de vida (puedes personalizar esto)
                if (collider.TryGetComponent(out IDamagable vidaObjeto)) {
                    vidaObjeto.AplicarDanio(danio);
                }
            }
        }


        // Se muestra en el guizmos de Unity, el radio de explosión
        private void OnDrawGizmos() {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, radioDeExplosion);
        }
    }
}